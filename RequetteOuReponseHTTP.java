package fr.univ.saint.etienne.res;

public class RequetteOuReponseHTTP {
	
	/**
	 * Type de message : requette ou r�ponse
	 */
	private TypeMessage type ;
	
	/**
	 * L'ent�te HTTP.
	 */
	private String  header ;
	
	/**
	 * Le corps du message.
	 */
	private String body ;
	
	/**
	 * The transfer-length of a message is the length of the message-body as it appears in the message;
	 */
	private int length ;
	
	
	
	public static RequetteOuReponseHTTP FIN = new  RequetteOuReponseHTTP() ;
	
	public enum TypeMessage {
		Request , Response 
	}

}
