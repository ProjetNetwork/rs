package fr.univ.saint.etienne.res;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientSocket {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Socket clientSocket = null;
		BufferedReader in;
		try {
			clientSocket = new Socket(InetAddress.getLocalHost(),
					ServeurSocket.PORT_TCP);

			System.out.println(" Demande de connexion ");
			in = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
			
			String messageDuServeur = in.readLine() ;
			System.out.println (messageDuServeur) ;
			
			clientSocket.close();
		} catch (UnknownHostException e) {

			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
