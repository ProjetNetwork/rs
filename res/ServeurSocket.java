/**
 * 
 */
package fr.univ.saint.etienne.res;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Classe main . point d'entr�e du programme.
 * 
 */
public class ServeurSocket {

	/**
	 * Le PORT TCP � �couter.
	 */
	public static final int PORT_TCP = 8999;

	/**
	 * Le nombre max de connexions simultan�es.
	 */
	private static final int MAX_CONNEXION = 5;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ServerSocket severSocket = null;

		try {

			// creer le serveur
			// severSocket = new ServerSocket(PORT_TCP, MAX_CONNEXION);
			severSocket = new ServerSocket(PORT_TCP);

			System.out.println("Le serveur est � l'�coute du port " + PORT_TCP);

			// On cr�er un Thread ou on va traiter les diff�rente requette.
			Thread thr = new Thread(new TraitementRequette(severSocket));

			// d�marrage du Thread.
			thr.start();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	// }

	private static void traiterClient() {
		System.out.println("----");

	}

	
	/**
	 * 
	 * Classe Thread , qui est charg�e de traiter les requette.
	 * 
	 */
	static class TraitementRequette implements Runnable {

		// socket du serveur
		private ServerSocket serveurSok;
		// socket du client
		private Socket socket;
		// compteur de nombre de requette.
		private static int nbreClient = 1;

		private String request = null;

		public TraitementRequette(ServerSocket serveurSok) {
			super();
			this.serveurSok = serveurSok;
		}

		@Override
		public void run() {

			try {
				while (true) {
					// Recup�rer la socket client.
					socket = serveurSok.accept();
					request = recupererLaRequette(socket);
					System.out.println(request);

					// ecrire une reponse
					ecrireUneReponse(socket,request);

					

					 socket.close();
					nbreClient++;
				}
			} catch (IOException e) {

				e.printStackTrace();
			}

		}

		protected static String recupererLaRequette(Socket socket)
				throws IOException {
			BufferedReader in;
			String requette = null;
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			requette = in.readLine();

			return requette;

		}

		protected static void ecrireUneReponse(Socket socket, String str)
				throws IOException {
			PrintWriter out;
			StringBuilder sb = new StringBuilder() ;
			sb.append("Coucou , vous le " + nbreClient + " client . \n");
			sb.append("Votre requette etait : \n ");
			sb.append(str);
			// rec
			out = new PrintWriter(socket.getOutputStream());
			// On envoie une r�ponse � la requette.
			out.write(sb.toString()) ;
			
			out.flush();
		}
	}

}
