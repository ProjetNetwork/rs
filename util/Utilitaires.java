package fr.univ.saint.etienne.res.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import fr.univ.saint.etienne.res.RequetteOuReponseHTTP;

public class Utilitaires {
	
	public static String readLine(InputStream is) throws IOException {
		StringBuilder sb = new StringBuilder() ;
		
		String textDeInputStream = "";
		String [] splitContenuDuFicher = null ;
		
		if(is!=null){
			// on recup�re les byte de l'input stream
			byte [] contenuDuInputStream = new byte[is.available()];
			
			is.read(contenuDuInputStream);
			
			// on le transforme en text
			textDeInputStream = new String(contenuDuInputStream);
			
			// transformer le texte en tableau , dont chaque �l�ment est une ligne
			 splitContenuDuFicher = textDeInputStream.split("\n") ;
			 
			 
			 for(String ligne :splitContenuDuFicher ){
				 // enlever le retour � la ligne si il existe
				 
				 sb.append(ligne.replace("\r", " "));
			 }
		}
		
		return sb.toString() ;
	}
	
	
	public static void readFull(InputStream is , byte[] tableau) throws IOException {
		
		is.read(tableau, 0, tableau.length) ;
		
	}
	
	public static OutputStream produireRequestHttp (RequetteOuReponseHTTP request) {
		String str = "GET " ;
		return null ;
	}

}
