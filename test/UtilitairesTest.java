package fr.univ.saint.etienne.res.test;

import java.io.IOException;
import java.io.InputStream;

import fr.univ.saint.etienne.res.util.Utilitaires;

public class UtilitairesTest {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		// charger le fichier.
		InputStream is = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("requetteHTTP.txt");
		
		testLectureStreamEnLigne(is);
		
		//testLectureTableaudeByte(is);

	}

	private static void testLectureStreamEnLigne(InputStream is)
			throws IOException {
		String res = Utilitaires.readLine(is);
		System.out.println(res);
	}

	private static void testLectureTableaudeByte(InputStream is)
			throws IOException {
		byte[] tableau = new byte[20];

		Utilitaires.readFull(is, tableau);

		String str = new String(tableau);

		System.out.println(str);
	}

}
